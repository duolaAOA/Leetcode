# -*-coding:utf-8 -*-
"""
在一个二维数组中，每一行都按照从左到右递增的顺序排序，
每一列都按照从上到下递增的顺序排序。请完成一个函数，
输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
"""
# 每次都从右上角的数比较
class Solution:
    def find(self, arrary, target):
        if not arrary:
            return False

        row_num = len(arrary)
        col_num = len(arrary[0]) - 1

        i, j = 0, col_num
        while i < row_num and j >= 0:
            if arrary[i][j] < target:
                i += 1
            elif arrary[i][j] > target:
                j -= 1
            else:
                return True
        return False

if __name__ == '__main__':
    arrary = [
        [1, 2, 8, 9],
        [2, 4, 9, 12],
        [4, 7, 10, 13],
        [6, 8, 11, 15]
    ]
    findtarget = Solution()
    print(findtarget.find(arrary, 11))