# -*-coding:utf-8 -*-

class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    # 返回从尾部到头部的列表值序列，例如[1,2,3]
    def printListFromTailToHead(self, listNode):
        tmp_list = []
        cur = listNode
        while cur is not None:
            tmp_list.insert(0, cur.val)
            cur = cur.next
        return tmp_list



