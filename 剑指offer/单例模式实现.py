# -*-coding:utf-8 -*-

# 使用模块， 天然就是单例模式


# 使用__new__

class Singleton:
    _instannce = None
    def __new__(cls, *args, **kwargs):
        if not cls._instannce:
            cls._instannce = super(Singleton, cls).__new__(cls, *args, **kwargs)
        return cls._instannce


# 使用装饰器

from functools import wraps

def singleton(cls):
    instance = {}
    @wraps(cls)
    def getinstance(*args, **kwargs):
        if cls not in instance:
            instance[cls] = cls(*args, **kwargs)
        return instance[cls]
    return getinstance
@singleton
class MyClass:
    a = 1


