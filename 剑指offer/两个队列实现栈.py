# -*-coding:utf-8 -*-

class Stack:
    def __init__(self):
        self.queue_1 = []
        self.queue_2 = []

    def push(self, node):
        self.queue_1.append(node)

    def pop(self):
        if not self.queue_1:
            return None
        while len(self.queue_1) != 1:
            self.queue_2.append(self.queue_1.pop(0))
        self.queue_1, self.queue_2 = self.queue_2, self.queue_1
        return self.queue_2.pop()

if __name__=='__main__':
    times=5
    testList=list(range(times))
    testStock=Stack()
    for i in range(times):
        testStock.push(testList[i])
    print(testList)
    for i in range(times):
        print(testStock.pop(), ',', end='')