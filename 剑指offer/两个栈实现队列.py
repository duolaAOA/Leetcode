# -*-coding:utf-8 -*-

class Queue:
    def __init__(self):
        self.stack_1 = []
        self.stack_2 = []

    def push(self, node):
        self.stack_1.append(node)

    def pop(self):
        if not self.stack_2:
            if not self.stack_1:
                return None
            else:
                for i in range(len(self.stack_1)):
                    self.stack_2.append(self.stack_1.pop())
        return self.stack_2.pop()


if __name__=='__main__':
    times=5
    testList=list(range(times))
    testQueue=Queue()
    for i in range(times):
        testQueue.push(testList[i])
    print(testList)
    for i in range(times):
        print(testQueue.pop(),',',end='')   #  end=''可以让 print 输出不换行