# -*-coding:utf-8 -*-

"""
Given two binary trees, write a function to check if they are the same or not.

Two binary trees are considered the same if they are structurally identical and the nodes have the same value.


Example 1:

Input:     1         1
          / \       / \
         2   3     2   3

        [1,2,3],   [1,2,3]

Output: true
Example 2:

Input:     1         1
          /           \
         2             2

        [1,2],     [1,null,2]

Output: false
Example 3:

Input:     1         1
          / \       / \
         2   1     1   2

        [1,2,1],   [1,1,2]

Output: false
"""
# 判断两棵树是否相同， 每个结点都一致返回True

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution:
    def isSameTree(self, p, q):
        """
        :type p: TreeNode
        :type q: TreeNode
        :rtype: bool
        """
        """递归DFS遍历"""
        if not p and not q:
            return True
        if not p or not q:
            return False
        if p.val != q.val:
            return False
        return self.isSameTree(p.left, q.left) and self.isSameTree(p.right, q.right)

    def isSameTree2(self, p, q):
        """DFS with stack"""
        stack = [(p, q)]
        while stack:
            node_p, node_q = stack.pop()
            if not node_p and not node_q:
                continue
            elif None in [node_p, node_q]:
                return False
            elif node_p.val != node_q.val:
                return False
            stack.append((node_p.right, node_q.right))
            stack.append((node_q.left, node_q.left))
        return True

    def isSameTree3(self, p, q):
        """BFS queue"""
        queue = [(p, q)]
        while queue:
            node_p, node_q = queue.pop()
            if not node_p and not node_q:
                continue
            elif None in [node_q, node_p]:
                return False
            elif node_p.val != node_q.val:
                return False
            queue.append((node_p.left, node_q.left))
            queue.append((node_p.right, node_q.right))
        return True


if __name__ == '__main__':
    p = [2, 1]
    q = [2, 1]
    tree_p = TreeNode(p)
    tree_q = TreeNode(q)
    a = Solution()
    print(a.isSameTree2(tree_p, tree_q))



