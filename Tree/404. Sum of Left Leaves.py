# -*-coding:utf-8 -*-
"""
Find the sum of all left leaves in a given binary tree.

Example:

    3
   / \
  9  20
    /  \
   15   7

There are two left leaves in the binary tree, with values 9 and 15 respectively. Return 24.

"""


# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def sumOfLeftLeaves(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0

        count = 0
        if root.left:
            if not root.left.left and not root.left.right:
                count += root.left.val

            else:
                count += self.sumOfLeftLeaves(root.left)

        count += self.sumOfLeftLeaves(root.right)
        return count


class Solution(object):
    def sumOfLeftLeaves(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0

        count = 0
        stack = [root]
        while stack:
            node = stack.pop()
            if node.left:
                if not node.left.left and not node.left.right:
                    count += node.left.val
                else:
                    stack.append(node.left)
            if node.right:
                stack.append(node.right)

        return count