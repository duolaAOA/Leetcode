# -*-coding:utf-8 -*-

"""

Given a binary tree, return the bottom-up level order traversal of its nodes' values. (ie, from left to right, level by level from leaf to root).

For example:
Given binary tree [3,9,20,null,null,15,7],
    3
   / \
  9  20
    /  \
   15   7
return its bottom-up level order traversal as:
[
  [15,7],
  [9,20],
  [3]
]
"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def levelOrderBottom(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """

        res = []
        self.dfs(root, 0, res)
        return res

    def dfs(self, node, level, res):
        if node:
            if len(res) < level + 1:  # 判断结点是否在同一层
                res.insert(0, [])
            res[-(level + 1)].append(node.val)  # 对每一层的结点加入列表
            self.dfs(node.left, level + 1, res)
            self.dfs(node.right, level + 1, res)

    def levelOrderBottom2(self, root):
        # DFS with stack
        stack = [(root, 0)]
        res = []
        while stack:
            node, level = stack.pop()
            if node:
                if len(res) < level + 1:
                    res.insert(0, [])
                res[-(level+1)].append(node.val)
                stack.append((node.right, level+1))
                stack.append((node.left, level+1))
        return res

    def levelOrderBottom3(self, root):
            # BFS with queue
            queue = [(root, 0)]
            res = []
            while queue:
                node, level = queue.pop()
                if node:
                    if len(res) < level + 1:
                        res.insert(0, [])
                    res[-(level + 1)].append(node.val)
                    queue.append((node.left, level + 1))
                    queue.append((node.right, level + 1))
            return res