# -*-coding:utf-8 -*-
"""
Given a binary tree, return all root-to-leaf paths.

For example, given the following binary tree:

   1
 /   \
2     3
 \
  5
All root-to-leaf paths are:

["1->2->5", "1->3"]
"""


class Solution(object):
    def binaryTreePaths(self, root):
        """
        :type root: TreeNode
        :rtype: List[str]
        """
        if not root:
            return []

        stack = [(root, "")]
        res = []

        while stack:
            node, strr = stack.pop()
            if not node.left and not node.right:
                res.append(strr + repr(node.val))
            if node.right:
                stack.append((node.right, strr + repr(node.val) + "->"))
            if node.left:
                stack.append((node.left, strr + repr(node.val) + "->"))
        return res