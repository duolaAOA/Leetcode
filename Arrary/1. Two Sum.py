# -*-coding:utf-8 -*-

"""
Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

Example:
Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].
"""


class Solution(object):
    # O(n^2)暴力破解，Runtime: 6115 ms没超时~~~
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        for i in range(len(nums)):
            for j in range(i+1, len(nums)):
                if target - nums[j] == nums[i]:
                    return [i, j]

    def twoSum2(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        hash_table = {}
        for cursor, num in enumerate(nums):
            hash_table[num] = cursor
        for cursor, num in enumerate(nums):
            if target - num in hash_table and hash_table[target-num] != cursor:
                return [cursor, hash_table[target-num]]

    def twoSum3(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        hash_table = {}
        for cursor, num in enumerate(nums):
            if target - num in hash_table:
                return [cursor, hash_table[target-num]]
            else:
                hash_table[num] = cursor
