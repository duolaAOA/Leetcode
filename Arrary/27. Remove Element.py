# -*-coding:utf-8 -*-

"""
Given an array and a value, remove all instances of that value in-place and return the new length.

Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

The order of elements can be changed. It doesn't matter what you leave beyond the new length.

Example:

Given nums = [3,2,2,3], val = 3,

Your function should return length = 2, with the first two elements of nums being 2.
"""
# 给定一个列表，以及一个val值，删除列表中所有和val相等的值，返回新列表的长度


class Solution:
    def removeElement(self, nums, val):
        """
        :type nums: List[int]
        :type val: int
        :rtype: int
        """
        if not nums:
            return 0
        while val in nums:
            nums.remove(val)
        return len(nums)

    def removeElement2(self, nums, val):
        """
        当我赋给变量nums时本地测试数据没有问题， 但在lc提交出错
        nums[3,2,2,3]  val=3
        excepted:[2,2]
        output:[3,2]
        本地output:[2,2], 和预期一致
        当我更改变量为nums[:]， LC显示AC
        列表操作问题 https://stackoverflow.com/questions/7677275/list-assignment-with
        https://docs.python.org/3/tutorial/introduction.html#lists
        """
        nums[:] = [x for x in nums if x != val]
        return len(nums)

    def removeElement3(self, nums, val):
        if not nums:
            return 0
        i = 0
        while i < len(nums):
            if nums[i] == val:
                nums.remove(i)
            else:
                i += 1
        return len(nums)
