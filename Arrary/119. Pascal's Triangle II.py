# -*-coding:utf-8 -*-

"""

Given an index k, return the kth row of the Pascal's triangle.

For example, given k = 3,
Return [1,3,3,1].

Note:
Could you optimize your algorithm to use only O(k) extra space?
"""
# 给定索引，返回帕斯卡三角形的该行数据


class Solution:
    def getRow(self, rowIndex):
        """
        :type rowIndex: int
        :rtype: List[int]
        """
        if rowIndex < 0 or rowIndex == 0:
            return [1]

        pre = [1, 1]
        if rowIndex == 1:
            return pre

        for i in range(2, rowIndex + 1):
            cur = [1] * (i + 1)

            for j in range(1, i):
                cur[j] = pre[j - 1] + pre[j]  # 嵌套元素索引

            pre = cur
        return pre





