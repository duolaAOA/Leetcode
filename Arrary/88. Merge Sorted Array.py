# -*-coding:utf-8 -*-

"""
Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.

Note:
You may assume that nums1 has enough space (size that is greater or equal to m + n)
to hold additional elements from nums2. The number of elements initialized in nums1 and nums2 are m and n respectively.
"""
"""
两个长度分别为m,n的列表，nums1的空间来保存两个列表.可假定nums1的空间为m+n，足够存放两个列表
可以从末往前存放
"""


class Solution:
    def merge(self, nums1, m, nums2, n):
        """
        :type nums1: List[int]
        :type m: int
        :type nums2: List[int]
        :type n: int
        :rtype: void Do not return anything, modify nums1 in-place instead.
        """
        index_nums1 = m - 1
        index_nums2 = n - 1
        size = m + n - 1
        while index_nums1 >= 0 and index_nums2 >= 0:
            if nums1[index_nums1] > nums2[index_nums2]:
                nums1[size] = nums1[index_nums1]
                index_nums1 -= 1
                size -= 1
            else:
                nums1[size] = nums2[index_nums2]
                index_nums2 -= 1
                size -= 1
        while index_nums2 >= 0:
            nums1[size] = nums2[index_nums2]
            size -= 1
            index_nums2 -= 1
