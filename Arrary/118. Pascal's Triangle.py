# -*-coding:utf-8 -*-

"""
Given numRows, generate the first numRows of Pascal's triangle.

For example, given numRows = 5,
Return

[
     [1],
    [1,1],
   [1,2,1],
  [1,3,3,1],
 [1,4,6,4,1]
]
"""
# 给定行数， 返回有n行帕斯卡三角形


class Solution:
    def generate(self, numRows):
        """
        :type numRows: int
        :rtype: List[List[int]]
        """
        res = []
        if numRows == 0:
            return res

        res.append([1])
        if numRows == 1:
            return res

        for i in range(2, numRows + 1):
            pre = res[-1]
            cur = [1] * i
            for j in range(1, i - 1):
                cur[j] = pre[j - 1] + pre[j]  # 嵌套列表中的元素索引
            res.append(cur)

        return res