# -*-coding:utf-8 -*-

"""
Find the contiguous subarray within an array (containing at least one number) which has the largest sum.

For example, given the array [-2,1,-3,4,-1,2,1,-5,4],
the contiguous subarray [4,-1,2,1] has the largest sum = 6.
"""
# 找出一个数组中总和最大的连续子数组（至少含有一个数）


class Solution:
    def maxSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        max_sum = cur_sum = nums[0]
        for i in nums[1:]:
            cur_sum = max(i, cur_sum + i)    # 当前和与当前元素比较
            max_sum = max(cur_sum, max_sum)
        return max_sum

    def maxSubArray2(self, nums):

        size = len(nums)
        dp = [0 for i in range(size)]  # 对每个值记录
        res = dp[0] = nums[0]

        for i in range(1, size):
            dp[i] = max(nums[i], dp[i - 1] + nums[i])
            res = max(res, dp[i])

        return res