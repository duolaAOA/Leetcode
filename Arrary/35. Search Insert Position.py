# -*-coding:utf-8 -*-

"""
Given a sorted array and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.

You may assume no duplicates in the array.

Example 1:
Input: [1,3,5,6], 5
Output: 2

Example 2:
Input: [1,3,5,6], 2
Output: 1

Example 3:
Input: [1,3,5,6], 7
Output: 4

Example 4:
Input: [1,3,5,6], 0
Output: 0
"""
# 如果查找到返回index
# 否则返回插入的index


class Solution:
    def searchInsert(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        lenght = len(nums)
        if not nums:
            return 0
        start, last = 0, lenght - 1
        while start + 1 < last:
            mid = start + (last - start) // 2
            if nums[mid] < target:
                start = mid
            elif nums[mid] > target:
                last = mid
            else:
                return mid

        if nums[last] < target:  # 如果大于最后一个元素
            return last + 1
        elif nums[start] >= target:  # 如果小于左指针
            return start
        else:
            return last

