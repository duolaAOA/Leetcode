#-*-coding:utf-8-*-

'''
Given a stream of integers and a window size, calculate the moving average of all integers in the sliding window.

For example,
MovingAverage m = new MovingAverage(3);
m.next(1) = 1
m.next(10) = (1 + 10) / 2
m.next(3) = (1 + 10 + 3) / 3
m.next(5) = (10 + 3 + 5) / 3
'''
'''
直接使用队列， 当元素小于 size时添加，当大于 size时 使用pop删除队首元素，
再加入新元素
'''
class MovingAverage(object):
    def __init__(self, size):
        self.size = size
        self.queue = []

    def next(self, val):
        if len(self.queue) < self.size:
            self.queue.append(val)
        else:
            self.queue.pop(0)
            self.queue.append(val)

        return float(sum(self.queue)) / len(self.queue)
