# -*-coding:utf-8 -*-
"""
Implement int sqrt(int x).
Compute and return the square root of x.
x is guaranteed to be a non-negative integer.

Example 1:
Input: 4
Output: 2

Example 2:
Input: 8
Output: 2
Explanation: The square root of 8 is 2.82842..., and since we want to return an integer, the decimal part will be truncated.
"""
# 此题和367一样


class Solution:
    #
    def mySqrt(self, x):
        """
        :type x: int
        :rtype: int
        """
        # 二分法
        if x <= 0:
            return 0
        if x == 1:
            return True
        left, right = 0, x // 2
        while right >= left:
            middle = left + (right - left) // 2
            if middle * middle == x:
                return middle
            elif middle * middle > x:
                right = middle - 1
            else:
                left = middle + 1
        return right


    def mySqrt2(self, x):
        """
        牛顿迭代法
        Xn+1 = (Xn+n/Xn)/2
        """
        if x == 0:
            return 0
        res = x
        while res * res > x:
            res = (res + x //res) // 2
        return res

print(1110%26)
print(1110//26)