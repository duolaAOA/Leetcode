# -*-coding:utf-8 -*-

"""
Given two binary strings, return their sum (also a binary string).

For example,
a = "11"
b = "1"
Return "100".
"""

"""
注意：如果强制Int转换的话，假设字符串很长，将可能导致溢出,Python的特殊机制不会导致溢出，当越界时自动加空间
"""

class Solution(object):
    def addBinary(self, a, b):
        """
        :type a: str
        :type b: str
        :rtype: str
        """
        # 只有0，1 从末往前，满2进1， 设置一个临时变量保存进位
        if a == '':
            return b
        if b == '':
            return a
        res = ''
        index_a, index_b, num = len(a) - 1, len(b) - 1, 0
        while index_a >= 0 or index_b >= 0:
            num += int(a[index_a]) if index_a >= 0 else 0
            num += int(b[index_b]) if index_b >= 0 else 0
            if num % 2 == 0:  # 与运算
                res = '0' + res
            else:
                res = '1' + res
            num //= 2
            index_a, index_b = index_a - 1, index_b - 1
        if num == 1:  # 进位
            res = '1' + res
        return res

    # 此种方法会溢出，另外不推荐使用内置的功能
    def addBinary2(self, a, b):
        """
        :type a: str
        :type b: str
        :rtype: str
        """
        num = int(a, 2) + int(b, 2)  # 第二个参数为转化的进制
        res = bin(num)[2:]  # convert int to binary string, Eg : bin(5)->'0b101'
        return res

