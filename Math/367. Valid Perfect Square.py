# -*-coding:utf-8 -*-
"""

Given a positive integer num, write a function which returns True if num is a perfect square else False.

Note: Do not use any built-in library function such as sqrt.

Example 1:

Input: 16
Returns: True
Example 2:

Input: 14
Returns: False
"""
# 0的平方根是0，但是leetcode结果0不是?


class Solution:
    def isPerfectSquare(self, num):

        """
        time: 因为是无限逼近，所以也不清楚
        sapce: 1
        牛顿迭代法
        Xn+1 = (Xn+n/Xn)/2
        """
        if num == 0:
            return False
        x = num
        while x * x > num:
            x = (x + num//x)//2
        return x * x == num

    def isPerfectSquare1(self, num):
        """
        # time: n  space: 1
        # 暴力求解
        """
        if num < 0:
            return False
        if num == 1:
            return True
        i, n = 1, num
        while i <= num:
            i += 1
            num = n // i
            if i * i == n:
                return True
        return False

    def isPerfectSquare2(self, num):
        """
        1 = 1
        4 = 1 + 3
        9 = 1 + 3 + 5
        16 = 1 + 3 + 5 + 7
        ...
        完全平方数为奇数之和
        """
        if num <= 0:
            return False
        i = 1
        while num > 0:
            num -= i
            i += 2
        return num == 0

    def isPerfectSquare4(self, num):
        """
        time: logN  space: 1
        二分法
        """
        if num == 0:
            return False
        if num == 1:
            return True
        left, right = 0, num//2
        while right >= left:
            middle = left + (right - left)//2
            if middle * middle == num:
                return True
            elif middle * middle > num:
                right = middle - 1
            else:
                left = middle + 1
        return False

    def isPerfectSquare5(self, num):
        """
        这样写也AC了
        """
        if num == 0:
            return False
        return True if int(num**0.5)*int(num**0.5) == num else False







