# -*-coding:utf-8 -*-

"""
Related to question Excel Sheet Column Title

Given a column title as appear in an Excel sheet, return its corresponding column number.

For example:

    A -> 1
    B -> 2
    C -> 3
    ...
    Z -> 26
    AA -> 27
    AB -> 28
    num = 1 * 26 + 2 =28

"""


class Solution:
    def titleToNumber(self, s):
        """
        :type s: str
        :rtype: int
        """
        num = i = 0
        while i < len(s):
            num = num * 26 + ord(s[i]) - ord('A') + 1
            i += 1
        return num

