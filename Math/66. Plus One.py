# -*-coding:utf-8 -*-

"""
Given a non-negative integer represented as a non-empty array of digits, plus one to the integer.

You may assume the integer do not contain any leading zero, except the number 0 itself.

The digits are stored such that the most significant digit is at the head of the list.
"""
# 此题描述不清，后查询得意思为：给定一个正整数的数组，如[1,2,3,4]代表整数1234，加1到1234（数组长度不变），得到1235
# 但[9,9,9,9]代表9999，加1到9999，得到10000


class Solution:
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        num = 0
        n = len(digits) - 1
        res = []
        for i in digits:
            if n == 0:
                num += i * 10 ** n + 1
            else:
                num += i * 10 ** n
            n -= 1
        for i in str(num):
            res.append(int(i))
        return res


    def plusOne2(self, digits):
        for i in range(len(digits)-1, -1, -1):
            if digits[i] != 9:
                digits[i] += 1
                break
            digits[i] = 0
        if digits[0] == 0:
            digits.insert(0, 1)
        return digits