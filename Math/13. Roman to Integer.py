# -*-coding:utf-8 -*-

"""

Given a roman numeral, convert it to an integer.

Input is guaranteed to be within the range from 1 to 3999.


"""
"""
罗马数字采用七个罗马字母作数字,即Ⅰ(1)、X(10)、C (100)、M (1000),V (5)、L(50)、D (500)
记数的方法:(1)相同的数字连写,所表示的数等于这些数字相加得到的数,如, Ⅲ = 3；
(2)小的数字在大的数字右边 = 左 +　右, 如,Ⅷ = 8,Ⅻ = 12；
(3)小的数字在左边 = 右 - 左　,如,Ⅳ = 4,Ⅸ = 9；

"""


class Solution:
    def romanToInt(self, s):
        """
        :type s: str
        :rtype: int
        """
        romans = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
        res, p = 0, 'I'
        for i in s[::-1]:
            res, p = res - romans[i] if romans[i] < romans[p] else res + romans[i], i
        return res
