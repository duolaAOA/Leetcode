# -*-coding:utf-8 -*-
"""
Find the nth digit of the infinite integer sequence 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ...

Note:
n is positive and will fit within the range of a 32-bit signed integer (n < 231).

Example 1:
Input:
3
Output:
3

Example 2:
Input:
11
Output:
0

Explanation:
The 11th digit of the sequence 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ... is a 0, which is part of the number 10.

"""
# 在无限整数序列，n <2^31。求出第N个数。eg:1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
# n = 11, 那么第10个数是10由1，0构成，所以第11个数是0
"""
从0到9的数字有一个数字，从10到99的数字有两个数字，从100到999的数字有3个数字，
依此类推。如果99 <n <999，那么你可以考虑改为n-99，然后把连续的数字串连起来
从100开始，直到你的串长度为n-99
1-9:9
10-99:90
100-999:900
1000-9999:9000
1 * 9
2 * 90
3 * 900
...
公式:
"""

class Solution:
    def findNthDigit(self, n):
        """
        :type n: int
        :rtype: int
        """
        start, size, step = 1, 1, 9
        while n > size * step:
            n -= (size * step)  # n不在范围内相减
            size += 1   # 表示当前处于的位数
            step *= 10  # 步长
            start *= 10     # 起始数字
        return int(str(start + (n - 1) // size)[(n - 1) % size])

















