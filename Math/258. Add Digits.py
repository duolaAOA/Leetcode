# -*-coding:utf-8 -*-

"""
Given a non-negative integer num, repeatedly add all its digits until the result has only one digit.

For example:

Given num = 38, the process is like: 3 + 8 = 11, 1 + 1 = 2. Since 2 has only one digit, return it.

Follow up:
Could you do it without any loop/recursion in O(1) runtime?
"""


class Solution:
    def addDigits(self, num):
        """
        :type num: int
        :rtype: int
        """
        n = 0
        while num != 0:
            n += num % 10
            num //= 10
        if n >= 10:
            return self.addDigits(n)
        else:
            return n

    def addDigits2(self, num):
        if num == 0:
            return 0
        return num % 9 or 9

