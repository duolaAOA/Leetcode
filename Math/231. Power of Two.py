# -*-coding:utf-8 -*-

"""
Given an integer, write a function to determine if it is a power of two.
"""
# 确定n是否为2的幂
# 2的幂为二进制位
# 2    10
# 4    100
# 8    1000
# 16   10000
# n    100000
# n-1  011111
# n & (n-1)为0

class Solution:
    def isPowerOfTwo(self, n):
        """
        :type n: int
        :rtype: bool
        """
        if n == 0:
            return False
        while n % 2 == 0:
            n //= 2
        return n == 1

    def isPowerOfTwo2(self, n):
        return (n > 0 and n & (n - 1) == 0)