# -*-coding:utf-8 -*-

"""
Given an integer n, return the number of trailing zeroes in n!.

Note: Your solution should be in logarithmic time complexity.
"""

# 给定一个整数N,判断阶乘产生的尾部0的个数
# 只有当 2 * 5 = 10，有0产生
# 10 9 8 7 6 5 4 3 2 1 可以拆分为2 和 5 逐一匹配，5可拆分出现的次数小于2，即可看5出现
# 多少次即可得到0的个数
# http://www.purplemath.com/modules/factzero.htm


class Solution:
    def trailingZeroes(self, n):
        """
        :type n: int
        :rtype: int
        """
        res = 0
        while n > 0:
            n //= 5
            res += n
        return res
