# -*-coding:utf-8 -*-

"""
Given a 32-bit signed integer, reverse digits of an integer.

Example 1:

Input: 123
Output:  321
Example 2:

Input: -123
Output: -321
Example 3:

Input: 120
Output: 21
Note:
Assume we are dealing with an environment which could only hold integers within the 32-bit signed integer range. For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.
"""


class Solution:
    # 边界范围： -2^31~2^31,或-2147483648~2147483648
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        # 此处比较真假返回0，1，-1
        s = (x > 0) - (x < 0)
        r = int(str(x*s)[::-1])
        if -2**31 < r < 2**31-1:
            return s*r
        else:
            return 0


    def reverse2(self, x):
        # 取模求反
        res = 0
        if x >=0:
            while x != 0:
                res = res *10 + x % 10
                x //= 10
            if res < 2**31-1:
                return res
            else:
                return 0
        else:
            x = -x
            while x != 0:
                res = res *10 + x % 10
                x //= 10
            if -res > -2**31:
                return -res
            else:
                return 0