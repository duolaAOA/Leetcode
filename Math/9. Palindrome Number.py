# -*-coding:utf-8 -*-

"""
Determine whether an integer is a palindrome. Do this without extra space.

Some hints:
Could negative integers be palindromes? (ie, -1)

If you are thinking of converting the integer to string, note the restriction of using extra space.

You could also try reversing an integer. However, if you have solved the problem "Reverse Integer", you know that the reversed integer might overflow. How would you handle such case?

There is a more generic way of solving this problem.
"""


class Solution:
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        # 注意不能占用额外空间, 但我不清楚我添加了两个临时变量是否算占用了额外空间，但是也AC了
        # Runtime: 601 ms
        # 此方法比第二种方法更耗时， 虽然第二种进行了类型转换
        if x < 0:
            return False
        res = 0
        re_x = x
        while re_x != 0:
            res = res * 10 + re_x % 10
            re_x //= 10
        return res == x

    def isPalindrome2(self, x):
        # Runtime: 584 ms
        x= str(x)
        return x[0::]==x[::-1]

    def isPalindrome3(self, x):
        # Runtime: 316 ms
        # n/2 次比较
        if x < 0:
            return False

        res = 0
        temp_x = x
        while temp_x != 0:
            res = res * 10 + temp_x % 10
            if res == 0:
                return False
            elif temp_x / res == 1.0:
                return True
            elif temp_x / res < 1.0:
                return temp_x == (res // 10)
            temp_x //= 10
        return res == x
