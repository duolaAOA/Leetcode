# -*-coding:utf-8 -*-

"""
Description:
Count the number of prime numbers less than a non-negative number, n.
"""
# 找出 1-n 中素数的个数
# 厄拉多塞筛法  时间复杂度O(loglogN)

class Solution:
    def countPrimes(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n < 3:
            return 0
        primes = [True] * n
        primes[0] = primes[1] = False
        for i in range(2, int(n ** 0.5) + 1):
            if primes[i]:
                primes[i * i:: i] = [False] * len(primes[i * i:: i])
        return sum(primes)

