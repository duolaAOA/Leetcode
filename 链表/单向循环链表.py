#-*-coding:utf-8-*-


class Node(object):
    '''节点'''
    def __init__(self, elem, next_=None):
        #_item存放数据元素
        self.elem = elem
        #_next是下一个节点标识
        self.next = next_


class SingLeCyclelinkList(object):
    '''单向循环链表'''
    def __init__(self, node=None):
        self.__head = node
        if node:
            node.next = node

    def is_empty(self):
        '''链表是否为空'''
        return self.__head is None

    def length(self):
        '''链表长度'''
        if self.is_empty():
            return 0
        # cur游标，用于移动遍历节点
        cur = self.__head
        # count用于记录数量
        count = 1
        while cur.next != self.__head:
            count += 1
            cur = cur.next
        return count

    def travel(self):
        '''遍历链表'''
        if self.is_empty():
            return
        cur = self.__head
        while cur.next != self.__head:
            print(cur.elem, end=" ")
            cur = cur.next
        # 退出循环cur指向尾结点，但尾结点元素未打印
        print(cur.elem)
        print("\n")

    def add(self, item):
        '''链表头部添加元素, 头插法'''
        node = Node(item)
        if self.is_empty():
            self.__head = node
            node.next = node
        else:
            cur = self.__head
            while cur.next != self.__head:
                cur = cur.next
            # 退出循环cur指向尾结点
            node.next = self.__head
            self.__head = node
            cur.next = node  # 1
            #cur.next = self.__head # 2   1与2表达一致

    def append(self, item):
        '''链表尾部添加元素'''
        node = Node(item)
        if self.is_empty():
            self.__head = node
            node.next = node
        else:
            cur = self.__head
            while cur.next != self.__head:
                cur = cur.next
            node.next = self.__head
            cur.next = node


    def insert(self, pos, item):
        '''指定位置添加元素'''
        # pos 从0开始
        if pos <= 0:
            self.add(item)
        elif pos > (self.length()-1):
            self.append(item)
        else:
            pre = self.__head
            count = 0
            while count < (pos-1):
                count += 1
                pre = pre.next
            #当循环退出后,pre指向pos-1位置
            node = Node(item)
            node.next = pre.next
            pre.next = node


    def remove(self, item):
        '''删除节点'''
        if self.is_empty():
            return
        cur = self.__head
        pre = None
        while cur.next != self.__head:
            if cur.elem == item:
                # 判断此结点是否为头结点
                if cur == self.__head:
                    # 头结点情况
                    # 找尾结点
                    rear = self.__head
                    while rear.next != self.__head:
                        rear = rear.next
                    self.__head = cur.next
                    rear.next = self.__head
                else:
                    pre.next = cur.next
                return
            else:
                pre = cur
                cur = cur.next
        # 退出循环cur指向尾结点
        if cur.elem == item:
            if cur == self.__head:
                # 链表只有一个结点
                self.__head = None
            else:
                pre.next = cur.next
                # pre.next = self.__head

    def search(self, item):
        '''查找节点是否存在'''
        if self.is_empty():
            return False
        cur = self.__head
        while cur.next != self.__head:
            if cur.elem == item:
                return True
            else:
                cur = cur.next
        # 退出循环cur指向尾结点
        if cur.elem == item:
            return True
        return False


if __name__ == "__main__":
    l1 = SingLeCyclelinkList()
    print(l1.is_empty())
    print(l1.length())
    l1.append(2)
    l1.add(8)
    l1.append(3)
    l1.append(4)
    l1.append(5)
    l1.append(6)
    # 8 1 2 3 4 5 6
    l1.insert(-1, 99) # 99 8 1 2 3 4 5 6
    l1.travel()
    l1.insert(3, 78) # 99 8 1 78 2 3 4 5 6
    l1.travel()
    l1.insert(12, 200) # 99 8 1 78 2 3 4 5 6 200
    l1.travel()
    l1.remove(8) # 99 1 78 2 3 4 5 6 200
    l1.travel()
    l1.remove(99)
    l1.travel() # 99 1 78 2 3 4 5 6