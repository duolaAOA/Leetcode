# -*-coding:utf-8 -*-
"""
Given an integer array nums, find the sum of the elements between indices i and j (i ≤ j), inclusive.

Example:
Given nums = [-2, 0, 3, -5, 2, -1]

sumRange(0, 2) -> 1
sumRange(2, 5) -> -1
sumRange(0, 5) -> -3
Note:
You may assume that the array does not change.
There are many calls to sumRange function.
"""

class NumArray:
    """
    不推荐， 虽然没超时
    runtime：1420 ms
    """
    def __init__(self, nums):
        """
        :type nums: List[int]
        """
        self.nums = nums

    def sumRange(self, i, j):
        """
        :type i: int
        :type j: int
        :rtype: int
        """
        return sum(self.nums[i:j+1])

class NumArray:

    def __init__(self, nums):
        self.dp = nums
        # sum[i+1] = sum[i] + nums[i+1]
        # sum[i] = sum[i-1] + nums[i]   i >= 1
        for num in range(1, len(nums)):
            self.dp[num] = self.dp[num - 1] + nums[num]

    def sumRange(self, i, j):

        return self.dp[j] - self.dp[i - 1] if i!=0 else self.dp[j]