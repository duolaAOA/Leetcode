# -*-coding:utf-8 -*-

"""

Given two strings s and t, write a function to determine if t is an anagram of s.

For example,
s = "anagram", t = "nagaram", return true.
s = "rat", t = "car", return false.

Note:
You may assume the string contains only lowercase alphabets.

Follow up:
What if the inputs contain unicode characters? How would you adapt your solution to such case?
"""


class Solution:
    # time: nlogn
    def isAnagram(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        sorted_s = "".join(sorted(s))
        sorted_t = "".join(sorted(t))
        return sorted_s == sorted_t


class Solution2:
    # 判断ascii的每个值是否相等
    def isAnagram(self, s, t):
        dic1 = [0] * 26
        dic2 = [0] * 26
        for item_s in s:
            dic1[ord(item_s) - 97] += 1
        for item_t in t:
            dic2[ord(item_t) - 97] += 1
        return dic1 == dic2

class Solution3:
    # time: nlogn
    def isAnagram(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        dic_s = {}
        dic_t = {}
        for item_s in s:
            dic_s[item_s] = dic_s.get(item_s, 0) + 1
        for item_t in t:
            dic_t[item_t] = dic_t.get(item_t, 0) + 1

        return dic_s == dic_t


class Solution4:
    def isAnagram(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        if len(s) != len(t):
            return False

        if set(s) != set(t):
            return False

        for ch in set(s):
            if s.count(ch) != t.count(ch):
                return False
        return True