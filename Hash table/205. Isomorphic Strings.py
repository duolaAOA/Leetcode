# -*-coding:utf-8 -*-

"""
Given two strings s and t, determine if they are isomorphic.

Two strings are isomorphic if the characters in s can be replaced to get t.

All occurrences of a character must be replaced with another character while preserving the order of characters. No two characters may map to the same character but a character may map to itself.

For example,
Given "egg", "add", return true.

Given "foo", "bar", return false.

Given "paper", "title", return true.

Note:
You may assume both s and t have the same length.
"""
# 判断两组字符串是否通过异形


class Solution:
    def isIsomorphic(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        if not s or not t:
            return True

        dic_s, dic_t = {}, {}
        size = len(s)

        for i in range(size):
            char_s = dic_s.get(s[i])
            char_t = dic_t.get(t[i])

            if not char_s and not char_t:
                dic_s[s[i]] = t[i]
                dic_t[t[i]] = s[i]

            elif char_s != t[i] or char_t != s[i]:
                return False
        return True

    def isIsomorphic2(self, s, t):
        # 也可用内置函数求
        return len(set(zip(s, t))) == len(set(s)) == len(set(t))