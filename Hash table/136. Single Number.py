# -*-coding:utf-8 -*-

"""

Given an array of integers, every element appears twice except for one. Find that single one.

Note:
Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
"""

# 找出给定数组中只出现一次的元素

class Solution:
    # 使用异或运算求解
    def singleNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        res = 0
        size = len(nums)
        for i in range(size):
            res ^= nums[i]
        return res

    def singleNumber2(self, nums):
        """
        建立字典存储
        """
        dic = {}
        for item in nums:
            try:
                dic.pop(item)
            except:
                dic[item] = 1
        return dic.popitem()[0]