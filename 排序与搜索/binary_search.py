# -*-coding:utf-8 -*-

# 最优 time: O(1)
# 最坏 time: O(logN)


def binary_search(alist, item):
    """
    二分查找,递归
    """
    length = len(alist)     # 表长
    if length > 0:
        mid = length // 2       # 中间元素
        if alist[mid] == item:
            return True
        elif item < alist[mid]:
            return binary_search(alist[:mid], item)
        else:
            return binary_search(alist[mid+1:], item)
    return False


def binary_search_2(alist, item):
    """
    二分查找，非递归
    """
    length = len(alist)
    first = 0
    last = length - 1
    while first < last:
        mid = (last - first)//2 + first
        if alist[mid] == item:
            return True
        elif item < alist[mid]:
            last -= 1
        else:
            first += 1
    return False


if __name__ == '__main__':
    li = [i for i in range(22, 77)]
    print(binary_search_2(li, 55))
    # print(binary_search(li, 55))

