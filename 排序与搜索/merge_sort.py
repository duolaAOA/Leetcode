# -*-coding:utf-8 -*-


def merge_sort(alist):
    """
    归并排序
    :param alist:
    :return:
    """
    if len(alist) <= 1:
        return alist
    mid = len(alist) // 2   # 列表中间位置
    left = merge_sort(alist[:mid])  # 归并排序左子列表
    right = merge_sort(alist[mid:])     # 归并排序右子列表
    return merge(left, right)


def merge(left, right):
    merged = []
    left_pointer, right_pointer = 0, 0      # 分别为left, right的下标
    left_len, right_len = len(left), len(right)     # 分别获取左右子列表的元素
    while left_pointer < left_len and right_pointer < right_len:    # 循环归并左右元素
        if left[left_pointer] <= right[right_pointer]:
            merged.append(left[left_pointer])       # 归并左子列表元素
            left_pointer += 1
        else:
            merged.append(right[right_pointer])      # 归并右子列表元素
            right_pointer += 1
    merged.extend(left[left_pointer:])      # 归并左子列表剩余元素
    merged.extend(right[right_pointer:])        # 归并右子列表剩余元素
    # print(left, right, merged)
    return merged       # 返回归并好的列表


if __name__ == '__main__':
    li = [12, 54, 6, 2, 554, 15, 661, 777, 1]
    sorted_list = merge_sort(li)
    print(li)
    print(sorted_list)
