# -*-coding:utf-8 -*-


def shell_sort(alist):
    """希尔排序"""
    n = len(alist)
    gap = n // 2
    while gap > 0:
        for j in range(gap, n):
            i = j
            while i > 0:
                if alist[i] < alist[i-gap]:
                    alist[i], alist[i-gap] = alist[i-gap], alist[i]
                    i -= gap
                else:
                    break
    # 缩短gap步长

        gap //= 2


if __name__ == '__main__':
    li = [12, 54, 6, 2, 554, 2266, 594, 31, 56, 6, 39, 66, 145, 78]
    shell_sort(li)
    print(li)
