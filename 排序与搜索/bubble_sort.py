# -*-coding:utf-8 -*-


def bubble_sort(alist):
    """冒泡排序"""

    for i in range(len(alist) - 1, 0, -1):
        found = False
        for j in range(i):
            if alist[j] > alist[j+1]:
                found = True
                alist[j], alist[j+1] = alist[j+1], alist[j]
        if not found:
            break


if __name__ == '__main__':
    li = [12, 54, 6, 2, 554, 2266, 594, 31, 56, 0, 39, 66, 145, 78]
    bubble_sort(li)
    print(li)
