# -*-coding:utf-8 -*-


def select_sort(alist):
    """选择排序"""
    # time O(n^2)
    n = len(alist)
    for j in range(n-1):  # 左闭右开， 实际为 j = n -2
        min_index = j
        for i in range(j + 1, n):
            if alist[min_index] > alist[i]:
                min_index = i
        alist[j], alist[min_index] = alist[min_index], alist[j]


if __name__ == '__main__':
    li = [12, 54, 6, 2, 554, 2266, 594, 31, 56, 6, 39, 66, 145, 78]
    select_sort(li)
    print(li)