# -*-coding:utf-8 -*-


def insert_sort(alist):
    n = len(alist)
    # 从右边的无序序列取出元素
    for j in range(1, n):
        i = j
        # 从右边的无序序列的首位取出，即i元素， 然后插入到前面有序序列
        while i > 0:
            if alist[i] < alist[i-1]:
                alist[i], alist[i-1] = alist[i-1], alist[i]
                i -= 1
            else:
                break


if __name__ == '__main__':
    li = [12, 54, 6, 2, 554, 2266, 594, 31, 56, 6, 39, 66, 145, 78]
    insert_sort(li)
    print(li)