# -*-coding:utf-8 -*-


def quick_sort(alist, first, last):
    """快速排序"""
    if first >= last:
        return
    pivot_value = alist[first]
    low = first
    high = last

    while low < high:
        # high左移
        while low < high and alist[high] >= pivot_value:
            high -= 1
        alist[low] = alist[high]
        # low 右移
        while low < high and alist[low] < pivot_value:
            low += 1
        alist[high] = alist[low]
    # 退出循环 low == high
    alist[low] = pivot_value

    # 递归前后半区
    quick_sort(alist, first, low-1)
    quick_sort(alist, low+1, last)


if __name__ == '__main__':
    li = [54, 26, 93, 17, 77, 31, 44, 55, 20]
    quick_sort(li, 0, len(li)-1)
    print(li)
