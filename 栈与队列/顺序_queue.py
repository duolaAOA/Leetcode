#-*-coding:utf-8-*-

"""
出队在队首，后面元素需要全部前移，将得到O(n)的时间操作，
若反过来在队尾弹出为O（1）,但是从队首插入的话，时间复杂度也为 O(n)

"""

class QueueUnderflow(ValueError):
    """队空"""
    pass


class SXQueue(object):
    """队列"""
    def __init__(self, init_len=8):
        self.__len = init_len
        self.__elems = [0] * init_len
        self.__num = 0

    def is_empty(self):
        return self.__num == 0

    def is_full(self):
        return self.__num == self.__len

    def peek(self):
        """查看队列最早进入的元素， 不删除"""
        if self.__num == 0:
            raise QueueUnderflow
        return self.__elems[self.__num - 1]

    def dequeue(self):
        # 入队
        if self.__num == 0:
            raise QueueUnderflow
        result = self.__elems[self.__num - 1]

        self.__num -= 1
        return result

    def enqueue(self, elem):
        # 出队
        if self.is_full():
            self.__extand()
        for i in range(self.__num, 0, -1):
            self.__elems[i] = self.__elems[i-1]
        self.__elems[0] = elem
        self.__num +=1

    def __extand(self):
        """队列满将其存储区加倍"""
        old_len = self.__len
        self.__len *= 2
        new_elems = [0] * self.__len
        for i in range(old_len):
            new_elems[i] = self.__elems[i]
        self.__elems = new_elems


if __name__ == '__main__':
    q = SXQueue()
    for i in range(10):
        q.enqueue(i)
    a = q.peek()
    for i in range(10):
        print (q.dequeue())
    print (q.is_empty())
