#-*-coding:utf-8-*-

'''
非 循环队列
出队在队首，后面元素需要全部前移，将得到O(n)的时间操作，
若反过来在队尾弹出为O（1）,但是从队首插入的话，时间复杂度也为 O(n)
'''

class QueueUnderflow(ValueError):
    '''队空'''
    pass

# 循环顺序表队列
# space :O(1)
# time O(1)
class SQueue(object):
    '''队列'''
    def __init__(self, init_len=8):
        self.__len = init_len               # 存储区长度
        self.__elems = [0] * init_len       # 元素存储
        self.__head = 0                     # 表头元素下标
        self.__num = 0                      # 元素个数

    def is_empty(self):
        '''判断队列是否为空，空时返回True都则False'''
        return self.__num == 0

    def enqueue(self, e):
        '''向队列添加元素,  入队'''
        if self.__num == self.__len:
            self.__extend()
        self.__elems[(self.__head + self.__num) % self.__len] = e   # 入队的下一个空位
        self.__num +=1

    def dequeue(self):
        '''删除队列最早进入的元素并将其删除，  出队'''
        if self.__num == 0:
            raise QueueUnderflow
        e = self.__elems[self.__head]
        self.__head = (self.__head + 1) % self.__len
        self.__num -= 1
        return e

    def peek(self):
        '''查看队列最早进入的元素， 不删除'''
        if self.__num == 0:
            raise QueueUnderflow
        return self.__elems[self.__head]

    def __extend(self):
        '''队列满将其存储区加倍'''
        old_len = self.__len
        self.__len *= 2
        new_elems = [0] * self.__len
        for i in range(old_len):
            new_elems[i] = self.__elems[(self.__head + i) % old_len]    #将原有元素迁移到新表
        self.__elems, self.__head = new_elems, 0


if __name__ == '__main__':
    q = SQueue()
    for i in range(10):
        q.enqueue(i)
    for i in range(10):
        print (q.dequeue())
    print (q.is_empty())