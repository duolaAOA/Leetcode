#-*-coding:utf-8-*-


'''
动态顺序表自动扩大存储区，但顺序表需要大块存储区，扩大一次需要高代价操作，链表操作在这上面具有优势，
但缺点是更依赖解释器的存储管理，每个结点开销，以及链接结点在实际计算机内存中任意散步可能带来的操作开销
'''

class StackUnderflow(ValueError):
    # 栈下溢  （空栈访问）
    pass


class LNode(object):
    def __init__(self, elem, top):
        self.elem = elem
        self.next = top


class LStack(object):
    '''基于链表技术实现栈类,用LNode作为结点'''
    def __init__(self):
        self.__top = None

    def is_empty(self):
        '''判断栈是否为空'''
        return self.__top is None

    def push(self, elem):
        '''将元素elem加入栈'''
        self.__top = LNode(elem, self.__top)

    def top(self):
        '''取得栈最后压入元素，不删除'''
        if self.__top is None:
            raise StackUnderflow("in LStack.top()")
        return self.__top.elem

    def pop(self):
        '''删除栈里最后压入的元素并将其返回，也称弹出'''
        if self.__top is None:
            raise StackUnderflow("in LStack.pop()")
        p = self.__top
        self.__top = p.next
        return p.elem

    def length(self):
        '''返回栈元素个数'''
        count = 0
        p = self.__top
        while p is not None:
            p = p.next
            count +=1
        return count


if __name__ == '__main__':
    s = LStack()
    for i in range(10):
        s.push(i)
    s.pop()
    while not s.is_empty():
        print(s.pop())