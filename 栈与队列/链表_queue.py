#-*-coding:utf-8-*-

'''
链表实现 具有头指针与尾指针 实现复杂度均为 O(1)
'''

class QueueUnderflow(ValueError):
    '''队空'''
    pass


class LNode(object):
    '''链表结点'''
    def __init__(self, elem, _next=None):
        self.elem = elem
        self.next = _next


class LQueue(object):
    '''链表实现队列'''
    def __init__(self):
        self.__head = None
        self.__rear = None

    def is_empty(self):
        return self.__head is None

    def peek(self):
        '''查看队列中最早进入的元素，不删除'''
        if self.is_empty():
            return QueueUnderflow
        return self.__head.elem

    def enqueue(self, elem):
        '''元素elem入队'''
        p = LNode(elem)
        if self.is_empty():
            self.__head = p
            self.__rear = p
        else:
            self.__rear.next = p
            self.__rear = p

    def dequeue(self):
        '''删除队列中最早进入的元素并返回   出队'''
        if self.is_empty():
            raise QueueUnderflow
        else:
            e = self.__head.elem
            self.__head = self.__head.next
            return e


