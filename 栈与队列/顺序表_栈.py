#-*-coding:utf-8-*-


'''
以List类型的数据属性_elems作为栈元素存储区，把_elems的首端作为栈底，尾端作为栈顶
'''

'''
动态顺序表自动扩大存储区，但顺序表需要大块存储区，扩大一次需要高代价操作，
'''

class StackUnderflow(ValueError):
    # 栈下溢  （空栈访问）
    pass


class SStack(object):
    '''
    基于顺序表实现的栈类
    用list对象_elems存储栈中元素
    所以栈操作都映射到list曹组
    '''
    ''' 栈 '''
    def __init__(self):
        self.__elems = []

    def is_empty(self):
        '''判断栈是否为空'''
        return self.__elems == []

    def top(self):
        '''返回栈顶元素,不删除'''
        if self.__elems == []:
            raise StackUnderflow("in SStack.top()")
        return self.__elems[-1]

    def push(self, elem):
        '''添加一个新元素elem（压栈）'''
        # 顺序表尾部插入时间复杂度  O(1)
        self.__elems.append(elem)

    def pop(self):
        '''弹出栈顶元素并将其返回'''
        return self.__elems.pop()

    def size(self):
        '''返回栈的元素个数'''
        return len(self.__elems)

if __name__ == '__main__':
    s = SStack()
    for i in range(10):
        s.push(i)
    while not s.is_empty():
        print(s.pop())
