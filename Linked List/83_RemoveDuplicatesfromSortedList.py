#-*-coding:utf-8-*-


'''
Given a sorted linked list, delete all duplicates such that each element appear only once.

For example,
Given 1->1->2, return 1->2.
Given 1->1->2->3->3, return 1->2->3.

'''


# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def deleteDuplicates(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        '''
        因为是有序链表，所以当前的和next值相同，删除next，直接指向next.next
        对于非有序排列的也可以先排序再对此操作
        '''
        if head is None or head.next == None:
            return head
        cur = head
        while cur.next is not None:
            if cur.next.val == cur.val:
                cur.next = cur.next.next
            else:
                cur = cur.next
        return head

s = Solution()
head = ListNode(0)
cur = head
ll = [1,5,5,5,5,4,4,6,7,9,22,5,45]
for i in ll:
    node = ListNode(i)
    cur.next = node
    cur = node
head = s.deleteDuplicates(head)
while(head != None):
    print(head.val, end=' ')
    head = head.next