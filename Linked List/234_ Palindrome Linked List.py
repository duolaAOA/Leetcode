#-*-coding:utf-8-*-


'''
Given a singly linked list, determine if it is a palindrome.

Follow up:
Could you do it in O(n) time and O(1) space?
'''
'''
1.分割链表
2.从中间分开的链表将后边的链表反转，与前一个链表比较是否相当
'''

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def isPalindrome(self, head):
        """
        :type head: ListNode
        :rtype: bool
        """
        slow = fast = head
        # find the mid node
        while fast and fast.next:
            fast = fast.next.next
            slow = slow.next
        # reverse the second half
        node = None
        while slow:
            temp = slow.next
            slow.next = node
            node = slow
            slow = temp
        # compare the first and second half nodes
        while node:
            if node.val !=head.val:
                return False
            node = node.next
            head = head.next
        return True












