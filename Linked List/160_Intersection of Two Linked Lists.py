#-*-coding:utf-8-*-

'''
Write a program to find the node at which the intersection of two singly linked lists begins.


For example, the following two linked lists:

A:          a1 → a2
                   ↘
                     c1 → c2 → c3
                   ↗            
B:     b1 → b2 → b3
begin to intersect at node c1.

Notes:

If the two linked lists have no intersection at all, return null.
The linked lists must retain their original structure after the function returns.
You may assume there are no cycles anywhere in the entire linked structure.
Your code should preferably run in O(n) time and use only O(1) memory.

'''

# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def getIntersectionNode1(self, headA, headB):
        """
        :type head1, head1: ListNode
        :rtype: ListNode
        """
        '''
        A: a1 → a2 → c1 → c2 → c3 →(None) b1 → b2 → b3 → c1 → c2 → c3
        B: b1 → b2 → b3 → c1 → c2 → c3 →(None) a1 → a2 → c1 → c2 → c3
        当A or B 为空，指向B or A结点
        time: O(m + n)
        space: O(1)
        '''
        if headA is None or headB is None:
            return None
        pa = headA
        pb = headB
        while pa is not pb:
            # if either pointer hits the end, switch head and continue the second traversal,
            # if not hit the end, just move on to next
            pa = headB if pa is None else pa.next
            pb = headA if pb is None else pb.next

        return pa  # only 2 ways to get out of the loop, they meet or the both hit the end=None

        # the idea is if you switch head, the possible difference between length would be countered.
        # On the second traversal, they either hit or miss.
        # if they meet, pa or pb would be the node we are looking for,
        # if they didn't meet, they will hit the end at the same iteration, pa == pb == None, return either one of them is the same,None


    def getIntersectionNode2(self, headA, headB):
        """
        :type head1, head1: ListNode
        :rtype: ListNode
        """
        '''
        A:          a1 → a2
                           ↘
                             c1 → c2 → c3
                           ↗            
        B:     b1 → b2 → b3
        # time:O(n)
        # space: O(1)
        前面多出的不必比较，比较后面部分相同的
        '''

        if headA is None or headB is None:
            return None
        lenA = self.len(headA)
        lenB = self.len((headB))
        if lenA > lenB:
            while lenA != lenB:
                headA = headA.next
                lenA -=1
        else:
            while lenA !=lenB:
                headB = headB.next
                lenB -=1

        while headA !=headB:
            headA = headA.next
            headB = headB.next
        return headA

    def len(self, head):
        length = 1
        while head is not None:
            head = head.next
            length +=1
        return length

