#-*-coding:utf-8-*-

'''
Reverse a singly linked list.
'''


'''
1 --> 2 --> 3 -->null
3 --> 2 --> 1 -->null

在遍历列表时，将当前节点的下一个指针改为指向其前一个元素。
由于一个节点没有引用它的前一个节点，所以你必须事先存储它的前一个元素。
在更改引用之前，还需要另一个指针来存储下一个节点。最后返回新的头！
'''


class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def reverseList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        pre = None
        while head is not None:
            temp = head.next
            head.next = pre
            pre = head
            head = temp
        return pre

s = Solution()
head = ListNode(0)
cur = head
for i in range(1,10):
    node = ListNode(i)
    cur.next = node
    cur = node
head = s.reverseList(head)
while head is not None:
    print(head.val, end=' ')
    head = head.next