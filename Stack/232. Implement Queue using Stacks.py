# -*-coding:utf-8 -*-

"""
Implement the following operations of a queue using stacks.

push(x) -- Push element x to the back of queue.
pop() -- Removes the element from in front of queue.
peek() -- Get the front element.
empty() -- Return whether the queue is empty.
Notes:
You must use only standard operations of a stack -- which means only push to top, peek/pop from top, size, and is empty operations are valid.
Depending on your language, stack may not be supported natively. You may simulate a stack by using a list or deque (double-ended queue), as long as you use only standard operations of a stack.
You may assume that all operations are valid (for example, no pop or peek operations will be called on an empty queue).

"""
# 此题换了很多写法都提示  Time Limit Exceeded 超时

#   我无语了，还是一样的代码我关闭了浏览器后再打开提交就AC了~~~

# 1
class MyQueue:

    def __init__(self):
        self.instack = []
        self.outstack = []

    def push(self, x):
        self.instack.append(x)

    def pop(self):
        if self.empty():
            return
        elif len(self.outstack) != 0:
            return self.outstack.pop()
        else:
            while len(self.instack) != 0:
                self.outstack.append(self.instack.pop())
            return self.outstack.pop()

    def peek(self):
        if self.empty():
            return
        elif len(self.outstack) != 0:
            return self.outstack[-1]
        else:
            while len(self.instack) != 0:
                self.outstack.append(self.instack.pop())
            return self.outstack[-1]

    def empty(self):
        return len(self.instack) == 0 and len(self.outstack) == 0

# 2
class MyQueue:

    def __init__(self):
        self.stack = []

    def push(self, x):
        self.stack.insert(0, x)

    def pop(self):
        return self.stack.pop()

    def peek(self):
        return self.stack[-1]

    def empty(self):
        return len(self.stack) == 0

# Your MyQueue object will be instantiated and called as such:
# obj = MyQueue()
# obj.push(2)
# param_2 = obj.pop()
# param_3 = obj.peek()
# param_4 = obj.empty()













