#-*-coding:utf-8-*-

'''
Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.

    push(x) -- Push element x onto stack.
    pop() -- Removes the element on top of the stack.
    top() -- Get the top element.
    getMin() -- Retrieve the minimum element in the stack.

Example:
    
    MinStack minStack = new MinStack();
    minStack.push(-2);
    minStack.push(0);
    minStack.push(-3);
    minStack.getMin();   --> Returns -3.
    minStack.pop();
    minStack.top();      --> Returns 0.
    minStack.getMin();   --> Returns -2.
'''

'''
1. 使用两个栈 stack保存用户数据，min_stack保存最小值
'''


class MinStack1(object):
    def __init__(self):
        self.stack = []

    def push(self, x):
        cur_min = self.getMin()
        if cur_min is None or x < cur_min:
            cur_min = x
        self.stack.append((x, cur_min))

    def pop(self):
        if self.stack is None:
            return None
        else:
            return self.stack.pop()

    def top(self):
        if len(self.stack) == 0:
            return None
        else:
            return self.stack[-1][0]

    def getMin(self):
        if len(self.stack) == 0:
            return None
        else:
            return self.stack[-1][1]

# Your MinStack object will be instantiated and called as such:


if __name__ == '__main__':
    obj = MinStack1()
    obj.push(2)
    obj.pop()
    param_3 = obj.top()
    param_4 = obj.getMin()












