# -*-coding:utf-8 -*-

"""
Implement a basic calculator to evaluate a simple expression string.

The expression string may contain open ( and closing parentheses ), the plus + or minus sign -, non-negative integers and empty spaces .

You may assume that the given expression is always valid.

Some examples:
"1 + 1" = 2
" 2-1 + 2 " = 3
"(1+(4+5+2)-3)+(6+8)" = 23
Note: Do not use the eval built-in library function.
"""


class Solution:
    def calculate(self, s):
        """
        :type s: str
        :rtype: int
        """
        mark = 1    # 符号标记
        res = 0     # 保存计算结果
        stack = []
        count = 0   # 下标
        while count < len(s):
            if s[count] == ' ':
                count += 1
                continue
            elif s[count].isdigit():
                num = ord(s[count]) - 48
                while (count + 1) < len(s) and (s[count + 1]).isdigit():
                    num = num * 10 + ord(s[count + 1]) - 48
                    count += 1
                res += num * mark
            elif s[count] == "+":
                mark = 1
            elif s[count] == "-":
                mark = -1
            elif s[count] == "(":
                stack.append(res)
                stack.append(mark)
                res = 0
                mark = 1
            elif s[count] == ")":
                res = res * stack.pop() + stack.pop()

            count += 1
        return res

