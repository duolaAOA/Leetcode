# -*-coding:utf-8 -*-

"""
Implement the following operations of a stack using queues.

push(x) -- Push element x onto stack.
pop() -- Removes the element on top of the stack.
top() -- Get the top element.
empty() -- Return whether the stack is empty.
Notes:
You must use only standard operations of a queue -- which means only push to back, peek/pop from front, size, and is empty operations are valid.
Depending on your language, queue may not be supported natively. You may simulate a queue by using a list or deque (double-ended queue), as long as you use only standard operations of a queue.
You may assume that all operations are valid (for example, no pop or top operations will be called on an empty stack).

"""


class MyStack1:

    def __init__(self):
        self.__elems = []

    def push(self, x):
        self.__elems.append(x)

    def pop(self):
        if self.empty():
            return None
        else:
            return self.__elems.pop()

    def top(self):
        if self.empty():
            return None
        else:
            return self.__elems[-1]

    def empty(self):
        return len(self.__elems) == 0


import collections


class MyStack2:
    """导入标准库collections, leetcode不需要导入"""
    def __init__(self):
        self._queue = collections.deque()

    def push(self, x):
        q = self._queue
        q.append(x)
        for _ in range(len(q) - 1):
            q.append(q.popleft())

    def pop(self):
        return self._queue.popleft()

    def top(self):
        return self._queue[0]

    def empty(self):
        return not len(self._queue)




# Your MyStack object will be instantiated and called as such:
# obj = MyStack()
# obj.push(2)
# param_2 = obj.pop()
# param_3 = obj.top()
# param_4 = obj.empty()
#



















