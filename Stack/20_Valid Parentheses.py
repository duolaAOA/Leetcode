#-*-coding:utf-8-*-


'''
Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

The brackets must close in the correct order, "()" and "()[]{}" are all valid but "(]" and "([)]" are not.

'''


class Solution:
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        stack = []
        opposite = {"]": "[", "}": "{", ")": "("}
        for i in s:
            if i in opposite.values():
                stack.append(i)
            elif i in opposite.keys():
                if stack == [] or opposite[i] != stack.pop():
                    return False
            else:
                return False
        return stack == []
















